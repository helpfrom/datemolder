<?php
// bootstrap.php
// Include Composer Autoload (relative to project root).
require_once "vendor/autoload.php";

use Datemolder\Formats\DatemolderBr;
use Datemolder\Formats\DatemolderFr;
use Datemolder\Formats\DatemolderUs;

$date = new DatemolderBr();
$dateFr = new DatemolderFr();
$dateUs = new DatemolderUs();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Date molder</title>
    </head>
    <body>
        <?php echo $date->formatDateTime(); ?>
        <?php echo $dateUs->formatDateTime(); ?>
        <?php echo $dateFr->formatDateTime(); ?>
    </body>
</html>