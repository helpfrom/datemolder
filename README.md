# README #

This examples use compose.

### What is this repository for? ###

* A easy way to implement a return format date for other countries.
* Version 1.0

### How do I get set up? ###

* Every Datemolder\Formats item has to set by "use Datemolder\Datemolder" to extend properly.

### Contribution guidelines ###

* Test over PHP5.5.12 and PHP5.6.17

```
#!php

use Datemolder\Formats\DatetimeBr;

$date = new Datemolder\Formats\DatetimeBr();

echo $date.'<br>';
echo $date->formatDate().'<br>';
echo $date->formatDateTime().'<br>';
echo $date->formatDateTimeDB().'<br>';
echo $date->formatDateDB().'<br>';

$date2 = Datemolder\Formats\DatetimeBr::fromFormat('Y-m-d','2015-10-15');
$date3 = Datemolder\Formats\DatetimeBr::fromFormat('Y-m-d H:i:s','2015-10-15 23:59:59');

```


### Who do I talk to? ###

* Samuel Prates <samuelprates@yahoo.com.br>