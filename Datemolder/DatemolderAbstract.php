<?php
namespace Datemolder;
/**
 * This class was made to extends PHP DateTime object
 * to better use of International developers
 */
abstract class DatemolderAbstract extends \DateTime{

    protected $outputDate       = 'Y-m-d';
    protected $outputDateTime   = 'Y-m-d H:i:s';

    /**
    * Get the format of date (without Hour) for usage in inserts
    * @return String String of formated date
    */
    public function getFormatDateDB(){
        return $this->format('Y-m-d');
    }

    /**
    * Get the format of date (with Hour) for usage in inserts
    * @return String String of formated date
    */
    public function getFormatDateTimeDB(){
        return $this->format('Y-m-d H:i:s');
    }

    /**
    * Get a new instance of this object from a format given
    * @return Object Instance Datemolder objeto
    */
    public static function fromFormat($format, $time){
        return new self(parent::createFromFormat($format, $time)->format('Y-m-d H:i:s'));
    }

    /**
    * Get the format of date (without Hour) set by $this->outputDate
    * @return String String of formated date
    */
    public function formatDate(){
        return $this->format($this->outputDate);
    }

    /**
    * Get the format of date (with Hour) set by $this->outputDate
    * @return String String of formated date
    */
    public function formatDateTime(){
        return $this->format($this->outputDateTime);
    }

    public function __toString() {
        return $this->formatDate();
    }

}